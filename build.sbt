name := "megafon-test"

version := "1.0"

scalaVersion := "2.11.12"



// https://mvnrepository.com/artifact/org.apache.spark/spark-core
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.2.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-hive
libraryDependencies += "org.apache.spark" %% "spark-hive" % "2.2.0" //% "provided"

// https://mvnrepository.com/artifact/com.github.scopt/scopt
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.1"
// https://mvnrepository.com/artifact/org.apache.commons/commons-text
libraryDependencies += "org.apache.commons" % "commons-text" % "1.6"

assemblyMergeStrategy in assembly := {
  case n if n.contains("services") => MergeStrategy.concat
  case n if n.startsWith("reference.conf") => MergeStrategy.concat
  case n if n.endsWith(".conf") => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) =>
    (xs map {_.toLowerCase}) match {
    case ("manifest.mf" :: Nil)  => MergeStrategy.discard
    case (x :: Nil) if (x.endsWith("rsa") || x.endsWith("dsa") || x.endsWith("sf"))=> MergeStrategy.discard
    case _ => MergeStrategy.first
  }
  //case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  //case PathList("META-INF", ps @ _*) => MergeStrategy.first
  case _ => MergeStrategy.last
}

mainClass in assembly := Some("ru.mgfn.test.home.Main")