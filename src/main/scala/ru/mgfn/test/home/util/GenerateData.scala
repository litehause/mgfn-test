package ru.mgfn.test.home.util

import java.time.{LocalDateTime, ZoneId, ZoneOffset}
import java.time.format.DateTimeFormatter
import java.util.Date

import org.apache.spark.sql.SparkSession
import ru.mgfn.test.home.dto.{PersonLogDTO, PlaceDTO}

import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import scala.util.Random

object GenerateData {
  private val startTime = LocalDateTime.of(2019, 1, 30, 23, 0)
  private val endtime = LocalDateTime.of(2019, 2, 10, 23, 0)
  private val dateTimeFormatWithHours = DateTimeFormatter.ofPattern("yyyyMMdd_HH")
  private val dateTimeFormat = DateTimeFormatter.ofPattern("yyyyMMdd")
  private val dateTimeFormatYearMonth = DateTimeFormatter.ofPattern("yyyyMM")

  def generateTables()(implicit sc: SparkSession): Unit = {
    generatePlacesTable()
    generateLogsTable()
  }

  private def generatePlacesTable()(implicit sc: SparkSession): Unit = {
    import sc.implicits._
    Seq(
      PlaceDTO(
      id = 1,
      name = "Государственный академический малый театр",
      category = "Театр",
      description = "Государственный академический малый театр на манежной",
      lat = 55.760176,
      lon = 37.619699,
      districtIdent = 1,
      data = "20190201"
    ),
      PlaceDTO(
        id = 2,
        name = "Музей Ю. В. Никулина",
        category = "Музей",
        description = "Музей Ю. В. Никулина на манежной",
        lat = 55.757666,
        lon = 37.634706,
        districtIdent = 1,
        data = "20190201"
      ),
      PlaceDTO(
        id = 3,
        name = "Государственная Третьяковская галерея",
        category = "Музей",
        description = "Государственная Третьяковская галерея",
        lat = 55.7413292,
        lon = 37.6206589,
        districtIdent = 1,
        data = "20190201"
      ),
      PlaceDTO(//DUBLICATE
        id = 4,
        name = "Государственная Третьяковская галерея",
        category = "Музей",
        description = "Государственная Третьяковская галерея",
        lat = 55.7413292,
        lon = 37.6206589,
        districtIdent = 1,
        data = "20190201"
      ),
      PlaceDTO(//Dublicate with error
        id = 5,
        name = "осударственная Третьяковская галерея",
        category = "Музей",
        description = "Государственная Третьяковская галерея",
        lat = 55.7413292,
        lon = 37.6206589,
        districtIdent = 1,
        data = "20190201"
      ),
      PlaceDTO(
        id = 6,
        name = "Эрмитаж",
        category = "Музей",
        description = "Один из музеев",
        lat = 59.939848,
        lon = 30.314568,
        districtIdent = 2,
        data = "20190201"
      )
    ).toDF()
      .write
      .format("parquet")
      .saveAsTable("places")
  }



  private def generateDate(from: LocalDateTime, to:LocalDateTime): LocalDateTime = {
    val startTime = from
      .toInstant(ZoneOffset.UTC)
      .toEpochMilli
    val different = to
      .toInstant(ZoneOffset.UTC)
      .toEpochMilli - startTime
    val time = startTime + Random.nextInt(different.toInt)
    LocalDateTime.ofInstant((new Date(time)).toInstant, ZoneId.systemDefault())
  }

  private def generateLogsTable()(implicit sc: SparkSession): Unit = {
    import sc.implicits._
    var increment = 0
    val personsLog = sc
      .table("places")
      .dropDuplicates("lat", "lon", "districtIdent")
      .collectAsList().toList
      .flatMap { item =>
        val lat = item.getAs[Double]("lat")
        val lon = item.getAs[Double]("lon")
        val districtIdent = item.getAs[Int]("districtIdent")
        (for (_ <- 0 to 1000) yield {
          increment = increment + 1
          generatePersonDTO(increment, lat, lon, districtIdent)
        }).toList
      }
    personsLog
      .toDF()
      .write
      .partitionBy("districtIdent", "yearMonth")
      .format("parquet")
      .saveAsTable("logs")


  }


  private def generatePersonDTO(increment: Int, lat: Double, lon: Double, districtIdent: Int): PersonLogDTO = {
    val signSymbool = if (Random.nextInt(10) > 5) {
      + 1
    }else {
      -1
    }
    val date = generateDate(startTime, endtime)
    PersonLogDTO(
      id = increment.toString,
      data_time = date.format(dateTimeFormatWithHours),
      lat = lat + (signSymbool * Random.nextInt(1000).toDouble/1000000),
      lon = lon + (signSymbool * Random.nextInt(1000).toDouble/1000000),
      districtIdent =  districtIdent,
      date = date.format(dateTimeFormat),
      yearMonth = date.format(dateTimeFormatYearMonth)
    )
  }


}

