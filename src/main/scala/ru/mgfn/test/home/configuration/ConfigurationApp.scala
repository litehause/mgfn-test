package ru.mgfn.test.home.configuration

case class ConfigurationApp(warehouseDir: String, maxDistance: Int)

object ConfigurationApp {
  def apply(args: Array[String]): ConfigurationApp = {
    new scopt.OptionParser[ConfigurationApp]("scopt") {
      opt[String]("warehouseDir").action((value, config) => {
        config.copy(warehouseDir = value)
      }).required()
      opt[Int]("maxDistance").action((value, config) => {
        config.copy(maxDistance = value)
      }).required()
    }.parse(args, ConfigurationApp(null, 0))
      .getOrElse(throw new Exception("error parse configuration"))

  }
}
