package ru.mgfn.test.home

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import ru.mgfn.test.home.configuration.ConfigurationApp
import ru.mgfn.test.home.util.GenerateData

object Main {

  def main(args: Array[String]): Unit = {

    val config = ConfigurationApp(args)

    implicit val sc = SparkSession.builder()
      .config("spark.master", "local")
//        .config("fs.file.impl",  classOf[org.apache.hadoop.fs.LocalFileSystem].getName())
        .config("spark.sql.warehouse.dir", config.warehouseDir)
      .getOrCreate()
    val conf = sc.sparkContext.hadoopConfiguration
    conf.set("fs.file.impl", classOf[org.apache.hadoop.fs.LocalFileSystem].getName)


    GenerateData.generateTables()
    val places = sc.table("places")
      .dropDuplicates("category", "lat", "lon", "districtIdent")
      .orderBy("id")

    places.show()

    val joinUDF = udf((logIdent: Int,
                     placeIdent: Int,
                     logLon: Double, logLat: Double,
                     placeLon: Double, placeLat: Double,
                     placeDate: String, logDate: String,
                     maxDistance: Int) => {
      logIdent == placeIdent &&
        Math.sqrt(Math.pow(placeLon - logLon, 2) + Math.pow(placeLat - placeLon, 2)) <= maxDistance &&
      placeDate == logDate
    })

    val logs = sc.table("logs")
    logs
      .alias("logs")
      .join(broadcast(places.alias("places")),
        joinUDF(logs.col("districtIdent"), places.col("districtIdent"),
          logs.col("lon"), logs.col("lat"),
          places.col("lon"), places.col("lat"),
          places.col("data"), logs.col("date"),
          lit(config.maxDistance)
        ),
        "left")
          .withColumn("recommendation", lit(0))
      .select(
        col("logs.id").alias("personId"),
        col("places.id").alias("placesId"),
        col("recommendation"),
        col("places.name").alias("placesName"),
        col("places.description").alias("placesDescription"),
        col("logs.lat").alias("lat"),
        col("logs.lon").alias("lon"),
        col("logs.districtIdent"),
        col("logs.date"))
      .show()
  }


}
