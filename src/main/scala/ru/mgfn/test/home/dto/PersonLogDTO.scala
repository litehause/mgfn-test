package ru.mgfn.test.home.dto

case class PersonLogDTO(id: String,
                        data_time: String,
                        lat: Double,//Широта
                        lon: Double,
                        districtIdent: Int,
                        date: String,
                        yearMonth: String
                       )
