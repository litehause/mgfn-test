package ru.mgfn.test.home.dto


case class PlaceDTO(id: Long,
                    name: String,
                    category: String,
                    description: String,
                    lat: Double,//Широта
                    lon: Double,
                    districtIdent: Int,
                    data: String)
